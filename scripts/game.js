﻿jewel.game = (function() {
    var dom = jewel.dom,
        $ = dom.$;

    /* ukrywa aktywny ekran i wyświetla ekran 
     * o podanym identyfikatorze. */
    function showScreen(screenId) {
        var activeScreen = $("#game .screen.active")[0],
            screen = $("#" + screenId)[0];
        if (activeScreen) {
            dom.removeClass(activeScreen, "active");
        }
        // Uruchamia moduł ekranów. 
        jewel.screens[screenId].run();
        // Wyświetla ekran.
        dom.addClass(screen, "active");
    }

    function setup() {
        // Wyłącza wbudowane zdarzenie przewijania dotykiem. 
        // Zapobiega przewijaniu strony. 
        dom.bind(document, "touchmove", function(event) {
            event.preventDefault();
        });
        // Chowa pasek adresów w Androidzie.
        if (/Android/.test(navigator.userAgent)) {
            $("html")[0].style.height = "200%";
            setTimeout(function() {
                window.scrollTo(0, 1);
            }, 0);
        }
    }
    
    // Wyłania metody publiczne.
    return {
        setup : setup,
        showScreen : showScreen
    };
})();
